import './Timer.scss'
import { FunctionComponent } from "react";

interface TimerProps {
    seconds: number
}

const Timer: FunctionComponent<TimerProps> = ({seconds: totalSeconds}) => {
    const seconds = totalSeconds % 60
    const minutes = Math.floor(totalSeconds / 60) % 60;
    const hours = Math.floor(totalSeconds / 60 / 60) % 24;
    const days = Math.floor(totalSeconds / 60 / 60 / 24) % 30;
    const months = Math.floor(totalSeconds / 60 / 60 / 24 / 30) % 12;
    const years = Math.floor(totalSeconds / 60 / 60 / 24 / 30 / 12);

    return <div className="timer">
        <span>
            <span><i>{years.toString().padStart(2, '0')}</i>&nbsp;years </span>
            <span><i>{months.toString().padStart(2, '0')}</i>&nbsp;months </span>
            <span><i>{days.toString().padStart(2, '0')}</i>&nbsp;days </span>
        </span>
        <span>
            <span>&nbsp;<i>{hours.toString().padStart(2, '0')}</i>&nbsp;hours </span>
            <span><i>{minutes.toString().padStart(2, '0')}</i>&nbsp;minutes </span>
            <span><i>{seconds.toString().padStart(2, '0')}</i>&nbsp;seconds </span>
        </span>

    </div>
}

export default Timer
