import './App.scss'
import Timer from "./countdown/Timer"
import { useEffect, useRef, useState } from "react"
import fildaHappy from './filda-happy.png'
import fildaSad from './filda-sad.png'

const COUNTDOWN_SECONDS = 60 * 60 * 24 * 30 * 12 * 2;
const SECONDS_TILL_RESET = 30;

const App = () => {
    const [seconds, setSeconds] = useState(COUNTDOWN_SECONDS + 3)
    const [isFildaHappy, setIsFildaHappy] = useState(false)
    const [isFildaResettingTimeout, setIsFildaResettingTimeout] = useState(false)
    const secondsRef = useRef(seconds)
    secondsRef.current = seconds

    const isFildaResettingTimeoutRef = useRef(isFildaResettingTimeout)
    isFildaResettingTimeoutRef.current = isFildaResettingTimeout

    useEffect(() => {
        let fildaHappinessTimeout: number | undefined = undefined
        let fildaDissapearTimeout: number | undefined = undefined
        let secondsResetTimeout: number | undefined = undefined;
        let secondInterval =
            setInterval(() => {
                if (!isFildaResettingTimeoutRef.current && (secondsRef.current + SECONDS_TILL_RESET) <= COUNTDOWN_SECONDS) {
                    setIsFildaResettingTimeout(true)
                    setIsFildaHappy(false)
                    secondsResetTimeout = setTimeout(() => setSeconds(COUNTDOWN_SECONDS), 7500)
                    fildaDissapearTimeout = setTimeout(() => setIsFildaResettingTimeout(false), 10000)
                    fildaHappinessTimeout = setTimeout(() => setIsFildaHappy(true), 7000)
                }

                setSeconds(secondsRef.current - 1)
            }, 1000)

        return () => {
            clearInterval(secondInterval)
            clearTimeout(fildaDissapearTimeout)
            clearTimeout(secondsResetTimeout)
            fildaDissapearTimeout = secondsResetTimeout = undefined
        }
    }, [])

    return <>
        <main className="app">
            <header>
                <svg viewBox="0 0 700 100">
                    <text x="50%" y="80%" textAnchor="middle">
                        <tspan>S</tspan>
                        <tspan>h</tspan>
                        <tspan>n</tspan>
                        <tspan>e</tspan>
                        <tspan>k</tspan>

                        <tspan>&nbsp;</tspan>

                        <tspan>t</tspan>
                        <tspan>o</tspan>
                        <tspan>o</tspan>
                        <tspan>l</tspan>

                        <tspan>&nbsp;</tspan>

                        <tspan>c</tspan>
                        <tspan>o</tspan>
                        <tspan>u</tspan>
                        <tspan>n</tspan>
                        <tspan>t</tspan>
                        <tspan>d</tspan>
                        <tspan>o</tspan>
                        <tspan>w</tspan>
                        <tspan>n</tspan>
                    </text>
                </svg>
            </header>
            <section className="countdown">
                <Timer seconds={seconds}/>
            </section>
        </main>
        <div className={`filda ${isFildaResettingTimeout ? 'filda--animate' : ''}`}>
            <div className="filda-image" style={{backgroundImage: isFildaHappy ? `url(${fildaHappy}), url(${fildaSad})` : `url(${fildaSad})`}}/>
        </div>

        <img className="hide" src={fildaHappy}/>
        <img className="hide" src={fildaSad}/>
    </>
}

export default App
